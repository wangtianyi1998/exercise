-- table中的函数
local func_table = {}

function func_table:func1()
   print('func1', self)
end
func_table.func1()
func_table:func1()
--[[
func1 nil
func1 table: 000002081D302130
]]


function func_table.func2(self)
       print('func2', self)
end
func_table.func2()
func_table:func2()
--[[
func2 nil
func2 table: 000002081D302130
]]


func_table.func3 = function(self)
   print('func3', self)
end
func_table.func3()
func_table:func3()
--[[
func3 nil
func3 table: 000002081D302130
]]
