----------------------------------------------------------------------
-- 变量
----------------------------------------------------------------------
a = 1             -- 全局变量
local b = 0.1     -- 局部变量
local c = 'I am a string'
print(a, b, c)    -- 1	0.1	I am a string
print(type(a), type(b), type(c))    -- number	number	string

b = 'I am a new string'
print(b)          -- I am a new string

a, b, c = 'a', 'b'  -- a, b, c = 'a', 'b', nil
print(a, b, c)    -- a  b  nil

local d = true
print(d, type(d))               -- true	boolean
