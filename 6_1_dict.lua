----------------------------------------------------------------------
-- 容器--字典
----------------------------------------------------------------------
-- 定义一个字典
dict = {1, 2, 3, name1='a', name2='b', mame3='c', name4='d', name5='e', name6='f'}
dict['name7'] = 'g'     -- dict.name7 = 'g'
print(dict['name1'], dict.name1, dict['name7'], dict.name7)       -- a a g g
print(#dict)         -- 3
dict['name1'] = nil
for i, v in pairs(dict) do
   print(i, dict[i], v)
end
--[[  
1	1	1
2	2	2
3	3	3
name4	d	d  每次运行不一样
name7	g	g
name6	f	f
name2	b	b
mame3	c	c
name5	e	e
]]

for i, v in ipairs(dict) do
   print(i, dict[i], v)
end
--[[
1	1	1
2	2	2
3	3	3
]]

-- table中的函数
local func_table = {}

function func_table:func1()
   print('func1', self)
end
func_table.func1()
func_table:func1()

function func_table.func2(self)
   print('func2', self)
end
func_table.func2()
func_table:func2()

func_table.func3 = function(self)
   print('func3', self)
end
func_table.func3()
func_table:func3()

