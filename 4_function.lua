----------------------------------------------------------------------
-- 函数
----------------------------------------------------------------------
a = 'imdt'          -- 全局变量
function f(n)       
    print(a)        -- imdt
    local a = n     -- 局部变量
    print(a)        -- 10
end
f(10)
print(a)            -- imdt

local fx = function(n) 
    print(a)        -- imdt
    local a = n     -- 局部变量
    print(a)        -- 100
end
fx(100)
