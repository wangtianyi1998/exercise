----------------------------------------------------------------------
-- 容器--数组
----------------------------------------------------------------------
-- 所有容器都是{}
arr = {'a', 1, 'b', 2, 'c', 3}     -- 定义一个数组
print(arr[0])       -- nil
print(arr[1])       -- a
print(arr[6])       -- 3
print(#arr)         -- 6
table.insert(arr, "imdt")
print(#arr)         -- 7
for i, v in ipairs(arr) do
   print(i, arr[i], v)
end
--[[
1	a	a
2	1	1
3	b	b
4	2	2
5	c	c
6	3	3
7	imdt	imdt
]]
table.remove(arr, 1)
print(#arr, arr[1])     -- 6	1
