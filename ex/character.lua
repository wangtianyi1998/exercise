Character = { timestamp = 0, id = -1, img = "", atk = 0, def = 0 }

function Character:new(img, atk, def)
    local newCharacter = {}
    setmetatable(newCharacter, self)
    self.__index = self;
    self.timestamp = self.timestamp + 1;
    newCharacter.id = self.timestamp;
    newCharacter.img = img
    newCharacter.atk = atk
    newCharacter.def = def
    print("Character created! id = "..newCharacter.id..", img = "..newCharacter.img..", atk = "..newCharacter.atk..", def = "..newCharacter.def..";")
    return newCharacter
end

return Character
