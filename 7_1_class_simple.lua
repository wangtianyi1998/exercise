----------------------------------------------------------------------
-- 类-简单版
----------------------------------------------------------------------
Hero = {}
Hero.hp = 100
Hero.name = 'hero1'
Hero.be_damaged = function(hero, lost_hp)
    hero.hp = hero.hp - lost_hp
    print(hero.name, 'hp', hero.hp)
end

Hero.new = function(hero, name, hp)
    local new_hero = {}
    new_hero.name = name
    new_hero.hp = hp
    setmetatable(new_hero, hero)     -- 设置原表
    hero.__index = hero               -- 设置__index
    return new_hero
end

hero2 = Hero.new(Hero, 'hero2')
hero2.be_damaged(hero2, 10)         -- hero2	hp	90

hero3 = Hero.new(Hero, 'hero3')
hero3.be_damaged(hero3, 5)          -- hero3	hp	95
