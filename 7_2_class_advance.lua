----------------------------------------------------------------------
-- 类-2
----------------------------------------------------------------------

Hero = {hp=100, name='hero1'}
function Hero:be_damaged(lost_hp)   -- function Hero.be_damaged(lost_hp, self)
    self.hp = self.hp - lost_hp
    print(self.name, 'hp', self.hp)
end

function Hero:new(name, hp)         -- function Hero.new(name, hp, self)
    local new_hero = {}
    new_hero.name = name
    new_hero.hp = hp
    setmetatable(new_hero, self)     -- 设置原表
    self.__index = self               -- 设置__index
    return new_hero
end

hero2 = Hero:new('hero2')    -- hero2 = Hero.new('hero2', Hero)
hero2:be_damaged(10)         -- hero2	hp	90    -- hero2.be_damaged(10, hero2)

hero3 = Hero:new('hero3')    -- hero3 = Hero.new('hero3', Hero)
hero3:be_damaged(5)          -- hero3	hp	95    -- hero3.be_damaged(5, hero3)

