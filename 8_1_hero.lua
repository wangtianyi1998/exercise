-- hero.lua
Hero = {hp=100, name='hero1'}
function Hero:be_damaged(lost_hp)
    self.hp = self.hp - lost_hp
    print(self.name, 'hp', self.hp)
end

function Hero:new(name, hp)    
    local new_hero = {}
    new_hero.name = name
    new_hero.hp = hp
    setmetatable(new_hero, self)     -- 设置原表
    self.__index = self              -- 设置__index
    return new_hero
end

return Hero